package com.ltlg.erplab.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ltlg.erplab.model.User;
import com.ltlg.erplab.repository.UserRepository;

@RestController
public class UserController {

	@Autowired
	private UserRepository repository;

	@GetMapping("/usuarios/{id}")
	public Optional<User> get(@PathVariable("id") int id) {
		return repository.findById(id);
	}

	// Bcrypt "usuarios", parche inmundo para que no se pueda acceder a los paths desde el deploy.
	@GetMapping("$2y$12$6KfOZEfpbKUQtZQzgdGkmuli79dafr0HfcRhUdXC0F888vL3yEIFG")
	public List<User> all() {
		return repository.findAll();
	}

	@PostMapping("/usuarios")
	public User add(@RequestBody User entity) {
		return repository.save(entity);
	}

	@DeleteMapping("/usuarios/{id}")
	public List<User> remove(@PathVariable("id") int id) {
		repository.deleteById(id);
		return repository.findAll();
	}

	@PutMapping("/usuarios")
	public User update(@RequestBody User entity) {
		return repository.save(entity);
	}
}
