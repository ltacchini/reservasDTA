package com.ltlg.erplab.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ltlg.erplab.model.Equipo;
import com.ltlg.erplab.model.Espacio;
import com.ltlg.erplab.model.Reserva;
import com.ltlg.erplab.service.ReservaService;

@RestController
public class ReservaController {

	@Autowired
	private ReservaService service;

	@GetMapping("/reservas/{id}")
	public Optional<Reserva> get(@PathVariable("id") int id) {
		return service.findById(id);
	}

	@GetMapping("/reservas")
	public List<Reserva> all() {
		return service.findAll();
	}

	@PostMapping("/reservas/equipos")
	public List<Equipo> equiposByDate(@RequestBody Reserva entity) {
		setTime(entity);
		return service.equiposDisponiblesByDate(entity);
	}

	@PostMapping("/reservas/espacios")
	public List<Espacio> espaciosByDate(@RequestBody Reserva entity) {
		setTime(entity);
		return service.espaciosDisponiblesByDate(entity);
	}

	@PostMapping("/reservas")
	public Reserva add(@RequestBody Reserva entity) {
		setTime(entity);
		return service.save(entity);
	}

	@DeleteMapping("/reservas/{id}")
	public List<Reserva> remove(@PathVariable("id") int id) {
		service.deleteById(id);
		return service.findAll();
	}

	@PutMapping("/reservas")
	public Reserva update(@RequestBody Reserva entity) {
		setTime(entity);
		return service.save(entity);
	}

	@SuppressWarnings({ "deprecation"})
	private void setTime(Reserva entity) {
		entity.getDia().setHours((entity.getDia().getHours() + 3));
	}
}