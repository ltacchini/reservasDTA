package com.ltlg.erplab.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ltlg.erplab.model.Equipo;
import com.ltlg.erplab.model.Espacio;
import com.ltlg.erplab.model.Reserva;
import com.ltlg.erplab.repository.EquipoRepository;
import com.ltlg.erplab.repository.EspacioRepository;
import com.ltlg.erplab.repository.ReservaRepository;

@Service
public class ReservaService {

	@Autowired
	private ReservaRepository repository;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private EspacioRepository espacioRepository;

	public Optional<Reserva> findById(int id) {
		return repository.findById(id);
	}

	public List<Reserva> findAll() {
		return repository.findAll();
	}

	public List<Equipo> equiposDisponiblesByDate(Reserva entity) {
		List<Equipo> ret = equipoRepository.findAll();
		for (Reserva r : repository.findAll()) {
			if (entity.seSolapa(r) && r.getIdReserva() != entity.getIdReserva()) {
				for (Equipo e : r.getEquipo()) {
					e.setActivo(false);
				}
			}
		}
		return ret;
	}

	public List<Espacio> espaciosDisponiblesByDate(Reserva entity) {
		List<Espacio> ret = espacioRepository.findAll();
		for (Reserva r : repository.findAll()) {
			if (entity.seSolapa(r) && r.getIdReserva() != entity.getIdReserva()) {
				if(r.getEspacio() != null) {
					r.getEspacio().setActivo(false);					
				}
			}
		}
		return ret;
	}

	public Reserva save(Reserva entity) {
		return repository.save(entity);
	}

	public void deleteById(int id) {
		repository.deleteById(id);
		;
	}

}