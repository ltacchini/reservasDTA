package com.ltlg.erplab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservasDTAApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservasDTAApplication.class, args);
	}
}
