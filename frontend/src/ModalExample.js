import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, ListGroup, ListGroupItem } from 'reactstrap';

class ModalExample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        const equipos = this.props.equipos;
        const Equipos = equipos.map(equipo => {
            const descripcion = equipo.nombre + ' - ' + equipo.descripcion + ' - ' + equipo.marca.descripcion
            return (
                <ListGroupItem key={equipo.id}>
                    {descripcion}
                </ListGroupItem>
            )
        });

        return (
            <div>
                <Button size='sm' color={this.props.color} onClick={this.toggle}>{this.props.buttonLabel}</Button>
                <Modal size='lg' isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Equipos</ModalHeader>
                    <ModalBody>
                        <ListGroup>
                            {Equipos}
                        </ListGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ModalExample;