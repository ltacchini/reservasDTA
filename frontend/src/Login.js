import React, { Component } from 'react';
import {
  Container, Col, Form,
  FormGroup, Label, Input,
  Button, FormText, FormFeedback,
} from 'reactstrap';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import './Login.css';


class Login extends Component {
emptyUser = {
    "id": 0,
    "email": "",
    "password": "",
    "name": "",
    "lastName": "",
    "active": 1,
    "roles": [
      {
        "role": "",
        "roleId": 0
      }
    ]
  }

  constructor(props) {
    super(props);
    this.state = {
      'usuario': '',
      'password': '',
      validate: {
        usuarioState: '',
      },
      'user': '',
    };
    this.state = {users:[]}
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch('$2y$12$6KfOZEfpbKUQtZQzgdGkmuli79dafr0HfcRhUdXC0F888vL3yEIFG')
        .then(response => response.json())
        .then(data => this.setState({ users: data }));
  }

  validateUsuario(e) {
    const { validate } = this.state;
    validate.usuarioState = 'has-success';
    this.setState({ validate });
  }

  handleChange = async (event) => {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [name]: value,
    });
  }

  submitForm(e) {
    e.preventDefault();

    var user = {
      "id": 0,
      "email": "",
      "password": this.state.password,
      "name": this.state.usuario,
      "lastName": "",
      "active": 1,
      "roles": [
        {
          "role": "",
          "roleId": 0
        }
      ]
    }
    var passwordHash = require('password-hash');
    var autenticado = false;
    for(var i=0; i < this.state.users.length;i++){
      if(this.state.usuario === this.state.users[i].name && 
        passwordHash.verify(this.state.password,this.state.users[i].password)){
          autenticado = true;
      }
    }
    if (autenticado){
      fetch('/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user,autenticado),
      })
        .then(response => response.json())
        .then(result => this.onSetResult(result, 'user'))
    }  
    else {
      localStorage.removeItem('user');
      NotificationManager.error('Usuario o contraseña incorrectos!', 'Acceso denegado', 5000, () => { });
    }  
  }

  onSetResult = (result, key) => {
    localStorage.setItem(key, JSON.stringify(result));
    this.setState({ user: result.user });
    this.props.history.push('/');
  }

  onError = (err, key) => {
    localStorage.removeItem(key);
    NotificationManager.error('Usuario o contraseña incorrectos!', 'Acceso denegado', 5000, () => { });
  }

  render() {
    const { usuario, password } = this.state;
    return (
      <Container className="Login">
      <div align="right">© 2019:
                    Lic. en Sistemas - IDEI - UNGS
                </div>
        <h2>Iniciar Sesión</h2>
                
        <Form className="form" onSubmit={(e) => this.submitForm(e)}>
          <Col>
            <FormGroup>
              <Label>Usuario</Label>
              <Input
                type="text"
                name="usuario"
                id="usuario"
                placeholder="Nombre de usuario"
                value={usuario}
                //valid={this.state.validate.usuarioState === 'has-success'}
                onChange={(e) => {
                  //this.validateUsuario(e)
                  this.handleChange(e)
                }}
                required
              />
              <FormFeedback>
                Usuario incorrecto
              </FormFeedback>
              <FormText>Ingrese su nombre de usuario</FormText>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="password">Contraseña</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Contraseña"
                value={password}
                onChange={(e) => this.handleChange(e)}
                required
              />
              <FormText>Ingrese su contraseña</FormText>
            </FormGroup>
          </Col>
          <Button color='primary'>Entrar</Button>
        </Form>
        <div>
          <NotificationContainer />
        </div>
      </Container>      
    );
  }
}

export default Login;