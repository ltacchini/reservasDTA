import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import ModalExample from './ModalExample'
import { Link } from 'react-router-dom';

class ReservarList extends Component {

    constructor(props) {
        super(props);
        this.state = { reserva: [], isLoading: true, user: '' };
        this.remove = this.remove.bind(this);
        const cachedUser = localStorage.getItem('user');
        this.state.user = JSON.parse(cachedUser);
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('/reservas')
            .then(response => response.json())
            .then(data => this.setState({ reserva: data, isLoading: false }));
    }

    async remove(id) {
        await fetch(`/reservas/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(() => {
            let updatedreserva = [...this.state.reserva].filter(i => i.idReserva !== id);
            this.setState({ reserva: updatedreserva });
        });
    }

    render() {
        const { reserva, isLoading, user } = this.state;
        let role;

        if (isLoading || user === '') {
            return <p>Cargando...</p>;
        }

        const ReservarList = reserva.map(reserva => {
            const activo = reserva.activo ? 'Activo' : 'Inactivo';
            const espacio = reserva.espacio === null ? '-' : reserva.espacio.nombre;
            const usuario = reserva.user.name + ' ' + reserva.user.lastName;

            let buttons;
            if (user !== undefined && reserva !== undefined) {
                role = user.roles[0].role;
                if (role === 'ADMIN' || (user.name === reserva.user.name)) {
                    buttons = <ButtonGroup>
                        <Button size="sm" color="info" tag={Link} to={"/reservar/" + reserva.idReserva}>Editar</Button>
                        <Button size="sm" color="info" onClick={() => this.remove(reserva.idReserva)}>Borrar</Button>
                    </ButtonGroup>;
                } else {
                    buttons = '';
                }
            } else {
                buttons = '';
            }

            return <tr key={reserva.idReserva}>
                <td>{usuario}</td>
                <td>{reserva.fecha}</td>
                <td>{reserva.desde.slice(-8, 5)}</td>
                <td>{reserva.hasta.slice(-8, 5)}</td>
                <td>{activo}</td>
                <td>{espacio}</td>
                <td>
                    <ModalExample color='info' buttonLabel='Ver equipos' equipos={reserva.equipo}></ModalExample>
                </td>
                <td>
                    {buttons}
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <div className="float-right">
                        <Button color="info" tag={Link} to="/reservar/new">Agregar reserva</Button>
                    </div>
                    <h3>Reservas</h3>
                    <Table>
                        <thead>
                            <tr>
                                <th width="25%">Usuario</th>
                                <th width="25%">Fecha</th>
                                <th width="15%">Desde</th>
                                <th width="15%">Hasta</th>
                                <th width="10%">Estado</th>
                                <th width="15%">Espacio</th>
                                <th width="10%">Equipos</th>
                                <th width="10%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {ReservarList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ReservarList;