import React from 'react';
import Select from 'react-select';

class EquipoComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
            selectedOption: null,
        }
    }

    handleChange = (selectedOption) => {
        const { handleChange, items } = this.props;
        this.setState({ selectedOption });
        let options = [];
        for (let e = 0; e < selectedOption.length; e++) {
            for (let i = 0; i < items.length; i++) {
                if (selectedOption[e].value === items[i].id) {
                    options.push(items[i]);
                }
            }
        }
        handleChange(options);
    }

    prepareOptions(items) {
        let options = [];
        for (let i = 0; i < items.length; i++) {
            let text = items[i].nombre + ' - ' + items[i].descripcion + ' - ' + items[i].marca.descripcion;
            options.push({
                value: items[i].id,
                label: text
            });
        }
        return options;
    }

    render() {
        const { selectedOption } = this.state;
        const { isMulti, items } = this.props;

        const options = this.prepareOptions(items);
        return (
            <Select
                placeholder="Equipos..."
                value={selectedOption}
                onChange={this.handleChange}
                options={options}
                isMulti={isMulti}
                isOptionDisabled={(option) => option.activo === true}
            />
        );
    }
}
export default EquipoComponent;